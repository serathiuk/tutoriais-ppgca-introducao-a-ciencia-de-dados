Cuba comemorarÃ¡ o primeiro aniversÃ¡rio da morte do emblemÃ¡tico lÃ­der Fidel Castro a partir de sÃ¡bado (25) com uma semana de vigÃ­lias no momento em que a ilha pÃµe em marcha um processo polÃ­tico que deve por fim aos 60 anos de governo dos irmÃ£os Castro.

Fidel, um sÃ­mbolo da esquerda revolucionÃ¡ria que construiu um Estado socialista Ã s portas dos Estados Unidos, desafiou durante dÃ©cadas os esforÃ§os de Washington para derrubÃ¡-lo, morrendo aos 90 anos em 25 de novembro de 2016.

O lÃ­der estava distante dos olhos do pÃºblico hÃ¡ uma dÃ©cada, perÃ­odo no qual foi substituÃ­do na PresidÃªncia por seu irmÃ£o RaÃºl a partir de 2008 depois de ser vitimado por uma doenÃ§a intestinal. Muitos cubanos acreditam que o paÃ­s mudou muito pouco desde seu falecimento.

O ritmo das reformas empreendidas por RaÃºl para atualizar o modelo econÃ´mico de estilo soviÃ©tico continuou com moderaÃ§Ã£o depois que o governo congelou, em agosto, a emissÃ£o de algumas licenÃ§as de trabalho no setor privado, ao mesmo tempo em que a relaÃ§Ã£o com os Estados Unidos piorou sob o comando do presidente Donald Trump.

Analistas argumentam que o mais significativo politicamente Ã© o ciclo eleitoral atual, que comeÃ§a no domingo com uma votaÃ§Ã£o municipal que terminarÃ¡ com a eleiÃ§Ã£o de um novo presidente do paÃ­s em fevereiro de 2018. RaÃºl, de 86 anos, reiterou que se retirarÃ¡ depois de cumprir dois mandatos sucessivos de cinco anos Ã  frente do governo.

Espera-se que a transiÃ§Ã£o no cargo seja gradual, jÃ¡ que RaÃºl continuarÃ¡ como chefe do governista Partido Comunista. Mas a sucessÃ£o chega em um momento difÃ­cil devido Ã  diminuiÃ§Ã£o da ajuda de sua aliada Venezuela, da fragilidade das exportaÃ§Ãµes e da falta de liquidez.

"Nosso futuro nem nÃ³s mesmos sabemos", disse Ariadna Valencia, professora de 45 anos de uma escola secundÃ¡ria. "RaÃºl termina seu mandato em 2018, Fidel jÃ¡ Ã© histÃ³ria e nÃ£o vejo realmente uma saÃ­da para melhorar a vida."

Antecipando-se Ã  sua morte, Fidel Castro havia reduzido ao mÃ­nimo suas apariÃ§Ãµes pÃºblicas, e escrevia ocasionalmente algumas colunas ou recebia algum dignitÃ¡rio prÃ³ximo em sua residÃªncia.

O desaparecimento do lÃ­der foi seguido por nove dias de luto nacional. Um cortejo fÃºnebre com suas cinzas fez uma viagem de trÃªs dias de Havana atÃ© a cidade de Santiago de Cuba, onde a revoluÃ§Ã£o cubana teve inÃ­cio.

Para manter seu desejo de evitar um culto Ã  personalidade, nÃ£o foram erguidas estÃ¡tuas de Fidel, e tampouco se batizou algum espaÃ§o pÃºblico em sua homenagem. AtÃ© sua sepultura tem um aspecto sÃ³brio, limitando-se a uma enorme pedra de granito em um cemitÃ©rio de Santiago de Cuba com uma placa que diz somente "Fidel".

