Anthony Garotinho deixa Bangu com festa de aliados e ataques a Cabral
Ex-governador nÃ£o usarÃ¡ tornozeleira, mas estÃ¡ impedido de deixar o paÃ­s. Presidente do PR tambÃ©m saiu da cadeia; os dois foram beneficiados por decisÃ£o de Gilmar Mendes.

O ex-governador do Rio de Janeiro Anthony Garotinho saiu do Complexo PenitenciÃ¡rio de GericinÃ³, em Bangu, na Zona Oeste, Ã s 20h30 desta quinta-feira (21). Ele foi beneficiado por uma decisÃ£o do ministro Gilmar Mendes, presidente do Tribunal Superior Eleitoral (TSE).

Um grupo de cerca de 20 pessoas, incluindo a filha dele, Clarissa Garotinho (PRB), fez festa na porta do presÃ­dio. ApÃ³s cumprimentos, beijos e abraÃ§os, ele conversou com jornalistas. "Nada vai mudar as minhas convicÃ§Ãµes", disse ele.

Anthony Garotinho e a mulher, a tambÃ©m ex-governadora Rosinha Matheus, foram presos no mÃªs passado por crimes eleitorais em uma aÃ§Ã£o da PolÃ­cia Federal. Os dois negam as acusaÃ§Ãµes.

Rosinha jÃ¡ respondia em liberdade. Garotinho agora tambÃ©m responderÃ¡ o processo em casa. NÃ£o precisarÃ¡ usar tornozeleira eletrÃ´nica, mas estÃ¡ impedido de deixar o paÃ­s â precisou entregar o passaporte Ã  JustiÃ§a.

Presidente do PR solto
Gilmar Mendes tambÃ©m mandou soltar o ex-ministro dos Transportes e presidente do PR, AntÃ´nio Carlos Rodrigues. Alvo da mesma operaÃ§Ã£o que Garotinho, ele chegou a ficar uma semana foragido, se entregou Ã  PolÃ­cia Federal, e estava em Benfica. Na noite desta quinta, ele deixou o presÃ­dio sem falar com jornalistas.

Antonio Carlos Ã© suspeito de negociar com o frigorÃ­fico JBS a doaÃ§Ã£o de dinheiro oriundo de propina para a campanha do ex-governador em 2014.

PolÃªmicas na prisÃ£o
Garotinho foi preso inicialmente na Cadeia PÃºblica JosÃ© Frederico Marques, mas saiu de lÃ¡ apÃ³s denunciar uma suposta agressÃ£o. No mesmo local estÃ¡ preso o ex-governador SÃ©rgio Cabral e os deputados estaduais Jorge Picciani, Paulo Melo e Edson Albertassi. Todos sÃ£o adversÃ¡rios polÃ­ticos de Anthony Garotinho.

Ele prestou queixa em uma delegacia do Rio e foi transferido para o presÃ­dio de Bangu 8, na Zona Oeste da cidade. Em depoimento, ele contou que adormeceu e foi despertado por um homem de 1,70m, branco, alourado, de calÃ§a jeans, sapato e blusa azul claro, com um bastÃ£o parecido com um taco de beisebol.

Segundo Garotinho, o homem teria dito: âVocÃª gosta muito de falar, nÃ£o Ã©?â. Ele relatou que logo depois, levou um golpe com o bastÃ£o no joelho.

O ex-governador contou, ainda, que o homem puxou uma pistola e disse: "Eu sÃ³ nÃ£o vou te matar para nÃ£o sujar para o pessoal aqui do lado", apontando em direÃ§Ã£o Ã  galeria onde estÃ£o os presos da Lava Jato.

O laudo do Instituto MÃ©dico Legal (IML) comprovou a existÃªncia de ferimentos no joelho e em um dos pÃ©s. As imagens de cÃ¢meras de seguranÃ§a da unidade, no entanto, nÃ£o mostraram nenhuma movimentaÃ§Ã£o.

"A hipÃ³tese de ediÃ§Ã£o nessas imagens Ã© bem difÃ­cil, nÃ£o vou dizer falaciosa. A resposta final serÃ¡ dos peritos", disse o delegado Wellington Vieira.

Ao ser questionado sobre a dificuldade de algum suposto agressor chegar atÃ© a cela, o delegado disse que a pessoa teria que passar por 12 portas. Anteriormente, alguns agentes penitenciÃ¡rios teriam falado em quatro portas.

"Na verdade, sÃ£o mais de quatro portas. Eu contei 12 portas desde a portaria atÃ© a cela onde ele estava, que Ã© a B4. Inclusive presos que estÃ£o na cela oposta vÃ£o ser inquiridos para que a gente consiga saber deles se os presos sentiram ou ouviram alguma coisa estranha", disse. Vieira ainda destacou que o sistema de seguranÃ§a Ã© bem feito que um possÃ­vel agressor teria que passar por muitas barreiras atÃ© chegar a cela de Garotinho.

O ex-secretÃ¡rio de SaÃºde do Rio de Janeiro SÃ©rgio CÃ´rtes e outros detentos tambÃ©m foram ouvidos sobre o caso.